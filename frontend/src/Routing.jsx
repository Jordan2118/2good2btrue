import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './App';
import Change from './Change'

const Routing = () => (
  <BrowserRouter>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/home" component={App} />
        <Route exact path="/change" component={Change} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default Routing;
