import React from 'react';
import 'w3-css/w3.css';

function Change() {

  function onClick(num){
    const formData = new FormData();
    formData.append('circleNumber', num)

    fetch('http://0.0.0.0:8080/api/percentage/set/', {
      method: 'POST',
      body: formData,
    })
      .then(res => res.json())
      .then((json) => {
        console.log(json)
      });
  }

  return (
    <div className="change">
      <button className="w3-button w3-center" style={{width: '100%', height: '33.33vh'}} onClick={()=>{onClick(1)}}>One</button>
      <button className="w3-button w3-center" style={{width: '100%', height: '33.33vh'}} onClick={()=>{onClick(2)}}>Two</button>
      <button className="w3-button w3-center"  style={{width: '100%', height: '33.33vh'}} onClick={()=>{onClick(3)}}>Three</button>
    </div>
  );
}

export default Change;
