from flask_restful import Resource, request, reqparse
import shelve
import random

class Percentage(Resource):
    def post(self):
        parser = reqparse.RequestParser()

        parser.add_argument("circleNumber", required=True)

        args = parser.parse_args()

        print(args['circleNumber'])

        shelf = shelve.open('percentage.db')

        shelf['number'] = args['circleNumber']
        shelf.close()
        return {'message': 'Success'}, 200

class GetPercentage(Resource):
    def get(self):

        shelf = shelve.open('percentage.db')

        if shelf['number']:
            num = int(shelf['number'])
            if num == 1:
                return {'message': 'Success', 'data': {'1': random.randint(65, 89), '2': random.randint(5, 32), '3': random.randint(5, 32)}}, 200
            if num == 2:
                return {'message': 'Success', 'data': {'1': random.randint(5, 32), '2': random.randint(65, 89), '3': random.randint(5, 32)}}, 200
            if num == 3:
                return {'message': 'Success', 'data': {'1': random.randint(5, 32), '2': random.randint(5, 32), '3': random.randint(65, 89)}}, 200
        else:
            return {'message': 'Invalid'}, 400
