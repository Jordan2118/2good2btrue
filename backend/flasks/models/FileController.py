from flask_restful import Resource, request
import os
from os import access, R_OK
from os.path import isfile
import subprocess
import time
import uuid



UPLOAD_FOLDER_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "..", "..", "data/")
)


UPLOAD_FOLDER_PATH_TMP = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "..", "tmp/")
)


class UploadFile(Resource):

    def convert_video(self, video_input, video_output):
        cmds = ['ffmpeg', '-y', '-i', video_input, video_output]
        subprocess.call(cmds)

    def post(self):
        f = request.files["file"]
        extension = os.path.splitext(f.filename)[1]

        print("Processing", f.filename)

        file_name_title = str(uuid.uuid4())[:8]
        file_name = file_name_title + extension
        file_name_mp4 = file_name_title + ".mp4"

        os.makedirs(os.path.join(UPLOAD_FOLDER_PATH_TMP), exist_ok=True)
        f.save(os.path.join(UPLOAD_FOLDER_PATH_TMP, file_name))

        file_dir = os.path.join(UPLOAD_FOLDER_PATH_TMP, file_name)

        while not isfile(file_dir) and access(file_dir, R_OK):
            print("File {} doesn't exist or isn't readable".format(file_dir))
            time.sleep(1)


        file_path = os.path.join(UPLOAD_FOLDER_PATH_TMP, file_name)
        file_path_final = os.path.join(UPLOAD_FOLDER_PATH, file_name_mp4)

        print("Converting file to type mp4")

        start_time = time.time()

        self.convert_video(file_path, "../data/" + file_name_mp4)

        end_time = time.time()

        print("Conversion took", end_time-start_time)
        print("Final product is located", file_path_final)

        import backend.cv.demo

        print(file_path_final)

        num = backend.cv.demo.main("../data/" + file_name_mp4)
        print("HERE", num)

        return {'message': 'Success', 'data':  num}