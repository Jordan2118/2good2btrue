import secrets
import json
import os
from flask import render_template, session, redirect, send_from_directory
from backend.flasks.models.FileController import *
from backend.flasks.models.Percentage import *

def init(app):
    app.register(UploadFile, '/api/file/upload/')
    app.register(Percentage, '/api/percentage/set/')
    app.register(GetPercentage, '/api/percentage/get/')
