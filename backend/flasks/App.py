from flask import Flask
import os
from flask_cors import CORS
import backend.flasks.models.model_manager
from flask_restful import Api

UPLOAD_FOLDER_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "..", "files/")
)
APP = None


class App:
    def __init__(self):
        self.app = Flask(
            __name__,
            static_folder="../../frontend/template/build/static",
            template_folder="../../frontend/template/build",
        )
        self.app.secret_key = "MYOOBERDOOPERSECRETKEYNONNEEWILLFINDOUTABOUTCAUSEGOOD"
        CORS(self.app)
        self.api = Api(self.app)
        self.app.url_map.strict_slashes = False
        self.app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER_PATH
        self.app.config["CORS_HEADERS"] = "Content-Type"
        global APP
        APP = self
        backend.flasks.models.model_manager.init(self)

    def register(self, resource, url):
        self.api.add_resource(resource, url)