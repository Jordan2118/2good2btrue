import sys
import subprocess
import os.path
import atexit
import signal
from waitress import serve
from backend.flasks.App import *

backgroundProcesses = []


def help_command():
    print("Invalid Parameters")
    print("- runserver --api_port 8081 --web_port 8080 debug")
    print("- createuser")


def check_pid(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True


def exit_handler():
    for process in backgroundProcesses:
        print("Ending Process:", process)
        os.kill(process, signal.SIGTERM)


atexit.register(exit_handler)


def execute(command, workspace):
    process = subprocess.Popen(
        command,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        cwd=workspace,
    )

    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == "" and process.poll() is not None:
            break
        sys.stdout.write(nextline)
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if exitCode == 0:
        return output
    else:
        print(command, exitCode, output)


if __name__ == "__main__":

    flask_dir = os.path.join(
        os.path.abspath(os.path.join(__file__, os.pardir)), "backend"
    )
    react_dir = os.path.join(
        os.path.join(os.path.abspath(os.path.join(__file__, os.pardir)), "frontend"),
        "template",
    )
    running = True

    debug = False

    args = sys.argv
    args.pop(0)

    if len(args) == 0:
        help_command()
    else:
        primary_command = args[0]
        args.pop(0)

        if primary_command == "runserver":
            error = False
            for i in range(0, len(args)):
                arg = args[i]
                if arg == "debug":
                    debug = True
            if error:
                help_command()
            print("Starting Server")
            if debug:
                print("Starting in Debug Mode")
                app = App()
                serve(app.app, host="0.0.0.0", port=8080)
            else:
                app = App()
                serve(app.app, host="0.0.0.0", port=80)
    while running:
        runningBackground = False
        for process in backgroundProcesses:
            if check_pid(process):
                runningBackground = True

        if not runningBackground:
            running = False
            print("Closing Server")
