import os
from moviepy.editor import VideoFileClip
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from uuid import uuid4

def main():
  path = "/home/spencer/Desktop/2good2btrue/backend/data/"
  videos = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and f[-4:] == '.mp4']
  for vid in videos:
    hidden = vid[0]
    vid_path = os.path.join(path, vid)

    full_vid = VideoFileClip(vid_path)
    duration = int(full_vid.duration)
    for start in range(duration):
      clip_name = path + 'clips/' + hidden + str(uuid4())[:8] + '.mp4'
      clip = full_vid.subclip(start, start + 1)
      clip.write_videofile(clip_name)


if __name__ == "__main__":
  main()
