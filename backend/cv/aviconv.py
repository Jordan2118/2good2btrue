import os
import subprocess
import time

path = '/home/spencer/Desktop/2good2btrue/backend/data/'
avis = [avi for avi in os.listdir(path+'avis/') if os.path.isfile(os.path.join(path+'avis/', avi))]

print(avis)

for avi in avis:
  cmds = ['ffmpeg', '-y', '-i', path+'avis/'+avi, '../data/'+avi[:10]+'.mp4']
  print(cmds)
  subprocess.call(cmds)
  time.sleep(1)
