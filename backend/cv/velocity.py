import shelve
import numpy as np

def main():

  s = shelve.open('frame.db')

  sorted_keys = sorted(s.keys())
  
  prev = None

  for key in sorted_keys:
    prev = None
    for frame in s[key]:
      frame_data = s[key][frame]
      if prev is None:
        prev = frame_data['center']
      else:
        current_center = frame_data['center']
        dx = current_center[0] - prev[0]
        dy = current_center[1] - prev[1]
        frame_data['vel'] = (dx, dy)

        shelf_data = s[key]
        shelf_data[frame] = frame_data
        s[key] = shelf_data

        prev = frame_data['center']

  s.close()
  s = shelve.open('frame.db')

  for key in s:
    print(key, s[key])






if __name__ == '__main__':
  main()
