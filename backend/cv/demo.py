import cv2, random
import numpy as np
import pandas as pd

def main(path):

  print(path)

  face_cascade = cv2.CascadeClassifier('./cascades/haarcascade_frontalface_default.xml')
  eye_cascade = cv2.CascadeClassifier('./cascades/haarcascade_eye.xml')

  face_thresh = 1.2
  face_weight = 5

  master_dict = {}

  cap = cv2.VideoCapture(path)

  frame_count = 0
  while True:
    ret, frame = cap.read()
    if not ret:
      break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, face_thresh, face_weight)
    for (x, y, w, h) in faces:
      cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
      roi_gray = gray[y:y+(h//2), x:x+w]
      roi_color = frame[y:y+(h//2), x:x+w]
      
      eyes = eye_cascade.detectMultiScale(roi_gray)
      if len(eyes) > 2:
        eyes = sorted(eyes, key=lambda eye: eye[2]*eye[3])
        eyes = eyes[:2]
      for (ex, ey, ew, eh) in eyes:
        cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
        eroi_gray = roi_gray[ey+10:ey+eh-10, ex:ex+ew]
        try:
          eroi_gray = cv2.resize(eroi_gray, (2*(ex+ew), 2*(ey+eh)), interpolation=cv2.INTER_AREA)
        except:
          _, eroi_gray = cv2.threshold(eroi_gray, 64, 255, cv2.THRESH_BINARY_INV)
        try:
          eroi_gray = cv2.erode(eroi_gray, None, iterations=2)
          eroi_gray = cv2.dilate(eroi_gray, None, iterations=4)
          eroi_gray = cv2.medianBlur(eroi_gray, 3)
          circles = cv2.HoughCircles(eroi_gray, cv2.HOUGH_GRADIENT, 1, 10, param1=50, param2=12, minRadius=10, maxRadius=200)
          if circles is not None and circles.any():
            circles = np.uint16(np.around(circles))
            x_total = 0
            y_total = 0
            for i in circles[0, :]:
              x_total += i[0]
              y_total += i[1]
            center = (x_total/len(circles[0, :]), y_total/len(circles[0, :]))
            master_dict[str(frame_count)] = {"center": center}
            frame_count += 1
        except:
          continue
  cap.release()
  cv2.destroyAllWindows()

  sorted_keys = sorted(master_dict.keys())

  prev = None
  for key in sorted_keys:
    current = master_dict[key]
    if prev is not None:
      dx = current["center"][0] - prev["center"][0]
      dy = current["center"][1] - prev["center"][1]
      current["speed"] = (dx ** 2 + dy ** 2)**0.5
      master_dict[key] = current
    prev = current
  
  total_speeds = 0
  counter = 0
  for key in sorted_keys:
    print(key)
    if "speed" in master_dict[key].keys():
      total_speeds += master_dict[key]["speed"]
      counter += 1
  
  if counter:
    avg_speed = total_speeds/counter
    if avg_speed > 79:
      return 65 + random.randint(-14,14)
    else:
      return 35 + random.randint(-14, 14)
  return 50