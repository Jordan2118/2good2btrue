import shelve
import numpy as np
import pandas as pd

def main():

  s = shelve.open('frame.db')
  frame_df = pd.DataFrame(columns=["clip", "num", "hidden", "posx", "posy", "velx", "vely", "speed"])
  # clip_df



  sorted_keys = sorted(s.keys())
  
  for key in sorted_keys:
    for frame in s[key]:
      frame_data = s[key][frame]
      if 'vel' in frame_data.keys():
        frame_df = frame_df.append({"clip": key, "num": frame, "hidden": frame_data["hidden"], "posx": frame_data["center"][0], "posy": frame_data["center"][1], "velx": frame_data["vel"][0], "vely": frame_data["vel"][1], "speed": (frame_data["vel"][0] ** 2 + frame_data["vel"][1] ** 2) ** 0.5}, ignore_index=True)

  print(frame_df.head(10))
  print(frame_df.describe())
  print("========================LIE===============================")
  print(frame_df[frame_df['hidden'] == "L"].describe())
  print("========================TRUE==============================")
  print(frame_df[frame_df['hidden'] == "T"].describe())



if __name__ == '__main__':
  main()
