import cv2, os
import numpy as np
import pandas as pd
import shelve

def main():
  
  s = shelve.open('frame.db')

  face_cascade = cv2.CascadeClassifier('./cascades/haarcascade_frontalface_default.xml')
  eye_cascade = cv2.CascadeClassifier('./cascades/haarcascade_eye.xml')

  detector_params = cv2.SimpleBlobDetector_Params()
  detector_params.filterByArea = True
  detector_params.maxArea = 1500
  detector = cv2.SimpleBlobDetector_create(detector_params)

  face_thresh = 1.2
  face_weight = 5

  path = "/home/spencer/Desktop/2good2btrue/backend/data/clips/"
  clips = [clip for clip in os.listdir(path) if os.path.isfile(os.path.join(path, clip)) and clip[-4:] == '.mp4']
  for i, clip in enumerate(clips):
    frames = {}
    cap = cv2.VideoCapture(path+clip)
    print("processing clip {} of {}: {}%".format(i, len(clips), int(100*i/len(clips))))
    
    frame_num = 0
    while True:
      ret, frame = cap.read()
      if not ret:
        break
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      
      faces = face_cascade.detectMultiScale(gray, face_thresh, face_weight)
      for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
        roi_gray = gray[y:y+(h//2), x:x+w]
        roi_color = frame[y:y+(h//2), x:x+w]
        
        eyes = eye_cascade.detectMultiScale(roi_gray)
        if len(eyes) > 2:
          eyes = sorted(eyes, key=lambda eye: eye[2]*eye[3])
          eyes = eyes[:2]
        for (ex, ey, ew, eh) in eyes:
          cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
          eroi_gray = roi_gray[ey+10:ey+eh-10, ex:ex+ew]
          try:
            eroi_gray = cv2.resize(eroi_gray, (2*(ex+ew), 2*(ey+eh)), interpolation=cv2.INTER_AREA)
          except:
            print("error @ {}".format(clip+str(frame_num)))
          _, eroi_gray = cv2.threshold(eroi_gray, 64, 255, cv2.THRESH_BINARY_INV)
          try:
            eroi_gray = cv2.erode(eroi_gray, None, iterations=2)
            eroi_gray = cv2.dilate(eroi_gray, None, iterations=4)
            eroi_gray = cv2.medianBlur(eroi_gray, 3)
            circles = cv2.HoughCircles(eroi_gray, cv2.HOUGH_GRADIENT, 1, 10, param1=50, param2=12, minRadius=10, maxRadius=200)
            if circles is not None and circles.any():
              circles = np.uint16(np.around(circles))
              x_total = 0
              y_total = 0
              for i in circles[0, :]:
                x_total += i[0]
                y_total += i[1]
              center = (x_total/len(circles[0, :]), y_total/len(circles[0, :]))
              frames[frame_num] = {'center': center, 'hidden': clip[0]}
              frame_num += 1
          except:
            print("==============================================================")
    s[clip] = frames

      


          # cv2.imshow("{}, {}".format(i, clip), eroi_gray)
    5      # cv2.waitKey()

    cap.release()
    cv2.destroyAllWindows()
    
#     if face_count > 15 and eye_count > 30:
#       df = df.append({'Clip': clip,
#                     'Hidden': clip[0]=='T',
#                     'Frame Count': frame_count,
#                     'Face Count': face_count,
#                     'Fa/F': face_count/frame_count,
#                     'Eye Count': eye_count,
#                     'E/F': eye_count/frame_count,
#                     'E/Fa': eye_count/face_count}, ignore_index=True)
# 
#   print("=======================PRESETS=====================")
#   print("face_thresh: {}, face_weight: {}".format(face_thresh, face_weight))
#   print("========================DATA=======================")
#   print(df.to_string())
#   print("----------------------------")
#   print(df.describe())


if __name__ == '__main__':
  main()
